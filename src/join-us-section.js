/* eslint-disable no-unused-expressions */
/* eslint-disable no-alert */
/* eslint-disable class-methods-use-this */

import validate from './email-validator.js';

const communityElement = document.querySelector('.community-section');
const joinUsElement = document.querySelector('.join-us-section');

class Section {
  constructor(title) {
    this.title = title;
    this.onloadHandler();
    this.isSubscribed = false;
  }

  displayCommunitySection = (employeesArray) => {
    const titleElement = document.createElement('h1');
    titleElement.setAttribute('slot', 'title');
    titleElement.innerHTML = ` 
        <h1 class="app-title" slot="title">Big Community Of <br>People Like You</h1>      
    `;
    const subtitleElement = document.createElement('h2');
    subtitleElement.setAttribute('slot', 'subtitle');
    subtitleElement.innerHTML = `
        <h2 class="app-subtitle">We’re proud of our products, and we’re really excited <br> when we get feedback from our users.</h2>
    `;

    const cardsElement = document.createElement('div');
    cardsElement.className = 'community-cards-containter';
    cardsElement.setAttribute('slot', 'other-content');
    cardsElement.innerHTML = employeesArray.map((employee, index) => `
        <div class="card">
          <img src="http://localhost:8080/api/avatars/avatar${index + 1}.png" alt="avatar${index + 1}" class="community-avatar-picture">
          <p class="card-employee-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</p>
          <div class="card-employee-name">${employee.firstName} ${employee.lastName}</div>
          <div class="card-employee-position">${employee.position}</div>
        </div>
    `).join('');
    communityElement.append(titleElement, subtitleElement, cardsElement);
  };

  displayJoinSection = () => {
    const titleElement = document.createElement('h1');
    titleElement.setAttribute('slot', 'title');
    titleElement.innerHTML = ` 
        <h1 class="app-title">${this.title}</h1>
    `;
    const subtitleElement = document.createElement('h2');
    subtitleElement.setAttribute('slot', 'subtitle');
    subtitleElement.innerHTML = ` 
        <h2 class="app-subtitle">Sed do eiusmod tempor incididunt 
        <br> ut labore et dolore magna aliqua.</h2>
    `;

    const formElement = document.createElement('form');
    formElement.className = 'join-subscribe-form';
    formElement.setAttribute('slot', 'other-content');
    formElement.innerHTML = `
        <label for="join-us-subscription"></label>
        <input 
        type="text" 
        id="join-us-subscription" 
        class=${localStorage.getItem('inputclassValue')}>
        <button class="app-section__button app-section__button--subscribe">
        ${!localStorage.getItem('buttonName') ? 'SUBSCRIBE' : localStorage.getItem('buttonName')}
        </button>
    `;
    joinUsElement.append(titleElement, subtitleElement, formElement);
    this.formElement = joinUsElement.querySelector('form');
    this.inputElement = joinUsElement.querySelector('input');
  };

  onloadHandler = () => {
    this.getCommunityRequest();
    this.displayJoinSection();

    this.pageLoadSpeed = performance.mark('pageEnd').startTime;
    performance.measureUserAgentSpecificMemory()
      .then((result) => this.sendAnalyticsData(result.bytes));

    this.myWorker = new Worker(
      new URL('./myWorker.js', import.meta.url),
      { name: 'my-worker' },
    );

    if (localStorage.getItem('subscriptionEmail')) {
      this.inputElement.value = localStorage.getItem('subscriptionEmail');
    }

    this.inputElement.addEventListener('input', this.inputHandler);
    this.formElement.addEventListener('submit', this.formSubmitHandler);
  };

  inputHandler = () => {
    localStorage.setItem('subscriptionEmail', this.inputElement.value);
  };

  formSubmitHandler = (event) => {
    event.preventDefault();

    localStorage.getItem('buttonName') === 'UNSUBSCRIBE'
      && (this.isSubscribed = true);

    if (!this.isSubscribed) {
      this.analytics();
      validate(this.inputElement.value)
        ? this.postSubscribeRequest()
        : alert('Please enter valid email address!');
      this.inputElement.focus();
    } else {
      this.postUnsubscribeRequest();
    }
  };

  getCommunityRequest = () => {
    const time1 = performance.now();
    fetch('http://localhost:8080/api/community')
      .then((response) => {
        const time2 = performance.now();
        this.communityRequestFetchingTime = time2 - time1;
        return response.json();
      })
      .then((data) => this.displayCommunitySection(data))
      .catch((error) => alert(error));
  };

  postSubscribeRequest = () => {
    this.analytics();
    this.isLoading(true);
    fetch('http://localhost:8080/api/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: localStorage.getItem('subscriptionEmail') }),
    })
      .then((response) => response.json())
      .then((data) => {
        data.error
          ? alert(data.error)
          : this.subscriptionActions();
        this.isLoading(false);
      })
      .catch((error) => alert(error));
  };

  postUnsubscribeRequest = () => {
    this.isLoading(true);
    fetch('http://localhost:8080/api/unsubscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: localStorage.getItem('subscriptionEmail') }),
    })
      .then(() => {
        this.unsubscriptionActions();
        this.isLoading(false);
      })
      .catch((error) => alert(error));
  };

  isLoading = (isLoadingNow) => {
    if (isLoadingNow) {
      joinUsElement.querySelector('button').disabled = true;
      joinUsElement.querySelector('button').style.opacity = '0.5';
    } else {
      joinUsElement.querySelector('button').disabled = false;
      joinUsElement.querySelector('button').style.opacity = '1';
    }
  };

  subscriptionActions = () => {
    localStorage.setItem('inputclassValue', 'no-display');
    localStorage.setItem('buttonName', 'UNSUBSCRIBE');
    localStorage.setItem('subscriptionEmail', '');
    this.inputElement.value = localStorage.getItem('subscriptionEmail');
    this.inputElement.className = 'no-display';
    joinUsElement.querySelector('button').textContent = 'UNSUBSCRIBE';
    this.isSubscribed = true;
  };

  unsubscriptionActions = () => {
    localStorage.setItem('inputclassValue', '');
    localStorage.setItem('buttonName', 'SUBSCRIBE');
    this.inputElement.className = '';
    joinUsElement.querySelector('button').textContent = 'SUBSCRIBE';
    this.inputElement.focus();
    this.isSubscribed = false;
  };

  analytics = () => {
    this.myWorker.postMessage(this.inputElement.value);
  };

  sendAnalyticsData = (pageMemoryUsage) => {
    const dataObjectsArray = [
      { communityRequestFetchingTime: this.communityRequestFetchingTime },
      { pageLoadSpeed: this.pageLoadSpeed },
      { pageMemoryUsage },
    ];

    fetch('http://localhost:8080/api/analytics/performance', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(dataObjectsArray),
    })
      // .then((response) => {
      //   console.log(response.ok);
      // })
      .catch((error) => alert(error));
  };
}

export default class SectionFactory {
  constructor(type) {
    this.type = type;
    this.switchSections();
  }

  switchSections() {
    switch (this.type) {
      case 'standard':
        return new Section('Join Our Program');
      case 'advanced':
        return new Section('Join Our Advanced Program');
      default:
        joinUsElement.remove();
    }
  }
}
