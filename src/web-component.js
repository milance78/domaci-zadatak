export default class WebsiteSection extends HTMLElement {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
  }

  connectedCallback() {
    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.innerHTML = `
      <slot name="logo"></slot>
      <slot name="title"></slot>
      <slot name="subtitle"></slot>
      <slot name="other-content"></slot>
      `;
  }
}
