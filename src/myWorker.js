/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */

let dataArray = [];

self.addEventListener('message', (event) => {
  dataArray = [...dataArray, event.data];
  if (dataArray.length === 5) {
    fetch('http://localhost:8080/api/analytics/user', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(dataArray),
    })
      // .then((response) => {
      //   console.log(response.ok);
      // })
      .catch((error) => alert(error));
    dataArray = [];
  }
});
