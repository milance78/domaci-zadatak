/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import { assert, expect } from 'chai';
import validate, { validateAsync, validateWithThrow } from '../email-validator.js';

describe('testing email validation', () => {
  it('string value "test@gmail.com" (ending gmail.com) should return true', () => {
    const actual = validate('test@gmail.com');
    const expected = true;
    expect(actual).to.deep.equal(expected);
  });
  it('string value "test@outlook.com" (ending outlook.com) should return true', () => {
    const actual = validate('test@outlook.com');
    const expected = true;
    expect(actual).to.deep.equal(expected);
  });
  it('string value "test@yandex.ru" (ending yandex.ru) should return true', () => {
    const actual = validate('test@yandex.ru');
    const expected = true;
    expect(actual).to.deep.equal(expected);
  });
  it('string value "test@wrongEnding" (ending different than yandex.ru, outlook.com and yandex.ru) should return false', () => {
    const actual = validate('test@wrongEnding');
    const expected = false;
    expect(actual).to.deep.equal(expected);
  });
  it('string value "123" should return false', () => {
    const actual = validate('123');
    const expected = false;
    expect(actual).to.deep.equal(expected);
  });
  it('string value " blank spaces around words " should return false', () => {
    const actual = validate(' blank spaces around words ');
    const expected = false;
    expect(actual).to.deep.equal(expected);
  });
  it('empty value "" should return false', () => {
    const actual = validate('');
    const expected = false;
    expect(actual).to.deep.equal(expected);
  });
});

describe('testing asynchroneous email validation', () => {
  it('string value "test@gmail.com" (ending gmail.com) should return true', (done) => {
    validateAsync('test@gmail.com')
      .then((response) => {
        const actual = response;
        const expected = true;
        expect(actual).to.deep.equal(expected);
        // using method 'done' for enabling asynchroneus testing. Waits for 2000ms
        done();
      });
  });
  it(
    'string value "test@outlook.com" (ending outlook.com) should return true',
    // putting tested function in a RETURN statement of IT func for enabling asynchroneus testing
    () => validateAsync('test@outlook.com')
      .then((response) => {
        const actual = response;
        const expected = true;
        expect(actual).to.deep.equal(expected);
      }),
  );
  it(
    'string value "test@yandex.ru" (ending yandex.ru) should return true',
    () => validateAsync('test@yandex.ru')
      .then((response) => {
        const actual = response;
        const expected = true;
        expect(actual).to.deep.equal(expected);
      }),
  );
  it(
    'string value "test@wrongEnding" (ending different than yandex.ru, outlook.com and yandex.ru) should return false',
    () => validateAsync('test@wrongEnding')
      .then((response) => {
        const actual = response;
        const expected = false;
        expect(actual).to.deep.equal(expected);
      }),
  );
  it(
    'string value "123" should return false',
    () => validateAsync('123')
      .then((response) => {
        const actual = response;
        const expected = false;
        expect(actual).to.deep.equal(expected);
      }),
  );
  it(
    'string value " blank spaces around words " should return false',
    () => validateAsync(' blank spaces around words ')
      .then((response) => {
        const actual = response;
        const expected = false;
        expect(actual).to.deep.equal(expected);
      }),
  );
  it(
    'empty string value "" should return false',
    () => validateAsync('')
      .then((response) => {
        const actual = response;
        const expected = false;
        expect(actual).to.deep.equal(expected);
      }),
  );
});

describe('testing email validation with throw', () => {
  it('string value "test@gmail.com" (ending gmail.com) should NOT throw error', () => {
    // the method is doesNotThrow()
    assert.doesNotThrow(() => {
      validateWithThrow('test@gmail.com');
    });
  });
  it('string value "test@outlook.com" (ending outlook.com) should NOT throw error', () => {
    assert.doesNotThrow(() => {
      validateWithThrow('test@outlook.com');
    });
  });
  it('string value "test@yandex.ru" (ending yandex.ru) should NOT throw error', () => {
    assert.doesNotThrow(() => {
      validateWithThrow('test@yandex.ru');
    });
  });
  // the method is throws()
  it('string value "test@wrongEnding" (ending different than yandex.ru, outlook.com and yandex.ru) should throw error', () => {
    assert.throws(() => {
      validateWithThrow('test@wrongEnding');
    });
  });
  it('string value "123" should should throw error', () => {
    assert.throws(() => {
      validateWithThrow('123');
    });
  });
  it('string value " blank spaces around words " should throw error', () => {
    assert.throws(() => {
      validateWithThrow(' blank spaces around words ');
    });
  });
  it('string value "" should throw error', () => {
    assert.throws(() => {
      validateWithThrow('');
    });
  });
});
