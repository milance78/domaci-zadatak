import SectionFactory from './join-us-section.js'; // eslint-disable-line import/extensions
import WebsiteSection from './web-component.js';
import './styles/style.css';

// 'standard' / 'advanced' / 'none'
window.onload = new SectionFactory('standard');

customElements.define('website-section', WebsiteSection);
