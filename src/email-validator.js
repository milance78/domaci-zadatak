/* eslint-disable no-restricted-syntax */
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export default (email) => {
  const ending = email.split('@')[1];

  for (const validEnding of VALID_EMAIL_ENDINGS) {
    if (ending === validEnding) { return true; }
  }

  return false;
};

export const validateAsync = (email) => {
  const ending = email.split('@')[1];

  for (const validEnding of VALID_EMAIL_ENDINGS) {
    if (ending === validEnding) {
      return Promise.resolve(true);
    }
  }
  return Promise.resolve(false);
};

export const validateWithThrow = (email) => {
  const ending = email.split('@')[1];

  for (const validEnding of VALID_EMAIL_ENDINGS) {
    if (ending === validEnding) { return true; }
  }
  throw new Error('email ending is invalid');
};
